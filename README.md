# Python / SQLite /SQLAlchemy

This repository contains an example of data mangement
with Python / SQLite / SQLAlchemy. Data are extracted from csv and structure in a relational database. The schema is made with sqlachemy. 

This project was built using Python

## Python Virtualenv

I use the `pyenv` tool to install Python versions on my Mac. I find it a very useful tool, and the instructions that follow use it, and are based on having Python version 3.8.0 installed using the following command:

```shell
$ pyenv install 3.8.0
```

## Installing The Project

From the main folder take the following steps:

- Install a Python virtual environment for this project
  - `pyenv local 3.8.0`
  - `python -m venv .venv`
- Activate the virtual environment
  - `source .venv/bin/activate`
- Install the project:

  - `python3 install -r requirements .`

  ## DATA and Schema
![image info](schema_dbeaver.png)


TODO: test and adapt using the postegres db from the docker container.
